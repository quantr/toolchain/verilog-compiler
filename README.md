# Goal

A verilog macro preprocessor, we are designing our own marco to support our RISC-V cpu development

# Macro

Auto wire two modules, need to base on the wire name standard

@{connect(moduleA, module B)}

Generate code to dump pin 1 information into our own trace format file

@{dump(moduleA.pin1)}

# CLI Usage

```
usage: java -jar verilog-compiler-xx.jar [OPTION] <input file>
 -h,--help                                help
 -j,--json <.v file>                      verilog to json
 -m,--macro <.v file>                     process macro in verilog file
 -o,--macro <output file>
 -p,--json <folder, top module .v file>   parse topmodule and return json
 -t <.v file or folder>                   compile verilog file,
                                          post-process it for our debugger
 -tvh <verilog heading statements>        work with -t option to
                                          post-process and inject verilog
                                          statement
 -tvs <verilog statements>                work with -t option to
                                          post-process and inject verilog
                                          statement
 -v,--version                             display version
Examples:
	1. java -jar target/verilog-compiler-2.0.jar -j ex.v
	1. java -jar target/verilog-compiler-2.0.jar -p ex.v
	1. java -jar target/verilog-compiler-2.0.jar -t src -tvh '' -o src-output
```

## java -jar verilogcompiler-xx.jar -j quantr_i.v

Result:

```
[ {
  "name" : "quantr_i",
  "inputs" : [ {
    "direction" : "input",
    "name" : "clk"
  }, {
    "direction" : "input",
    "name" : "rst"
  } ],
  "outputs" : [ {
    "direction" : "output",
    "range" : "[`MXLEN]",
    "name" : "pc"
  } ]
} ]
```

# Source code explain

Verilog preprocess is complex, here is the flow

1. run the verilog by -t command
1. VerilogHelper.preProcess()
1. String sourceContent = VerilogHelper.getContentReplacedWithMacro(1, content, VerilogHelper.scanMacroDefinitions(file, file.getParentFile()), ps);
1. ps.fatternMacroListing()

