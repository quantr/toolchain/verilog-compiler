package hk.quantr.verilogcompiler;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompiler {

	private static Logger logger = Logger.getLogger(VerilogCompiler.class.getName());

	static {
		InputStream stream = VerilogCompiler.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[]) throws ParseException, FileNotFoundException, IOException {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption(Option.builder("j")
				.required(false)
				.hasArg()
				.argName(".v file")
				.desc("verilog to json")
				.longOpt("json")
				.build());
		options.addOption(Option.builder("p")
				.required(false)
				.hasArg()
				.argName("folder, top module .v file")
				.desc("parse topmodule and return json")
				.longOpt("json")
				.build());
		options.addOption(Option.builder("m")
				.required(false)
				.hasArg()
				.argName(".v file")
				.desc("process macro in verilog file")
				.longOpt("macro")
				.build());
		options.addOption(Option.builder("o")
				.required(false)
				.hasArg()
				.argName("output file")
				.longOpt("macro")
				.build());
		options.addOption(Option.builder("t")
				.required(false)
				.hasArg()
				.argName(".v file or folder")
				.desc("compile verilog file, post-process it for our debugger")
				.build());
		options.addOption(Option.builder("tvh")
				.required(false)
				.hasArg()
				.argName("verilog heading statements")
				.desc("work with -t option to post-process and inject verilog statement")
				.build());
		options.addOption(Option.builder("tvs")
				.required(false)
				.hasArg()
				.argName("verilog statements")
				.desc("work with -t option to post-process and inject verilog statement")
				.build());
		options.addOption("h", "help", false, "help");

		if (args.length == 0 || Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar verilog-compiler-xx.jar [OPTION] <input file>", options);
			System.out.println("Examples:");
			System.out.println("\t1. java -jar target/verilog-compiler-2.0.jar -j ../quantr-i/src/ex.v");
			System.out.println("\t2. java -jar target/verilog-compiler-2.0.jar -p ../quantr-i/src/ex.v");
			System.out.println("\t2. java -jar target/verilog-compiler-2.0.jar -t ../quantr-i/src/ex.v");
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				logger.log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);
		if (cmd.hasOption("j")) {
			String temp = cmd.getOptionValue("j");
			File file = new File(temp);
			System.out.println(VerilogHelper.verilogToJson(file.getAbsoluteFile()));
		} else if (cmd.hasOption("p")) {
			File temp = new File(cmd.getOptionValue("p")).getAbsoluteFile();
			hk.quantr.verilogcompiler.structure.Module module = VerilogHelper.fileToModule(temp);
			System.out.println(CommonLib.objectToJson(module));
		} else if (cmd.hasOption("t")) {
//			if (!cmd.hasOption("tvh")) {
//				logger.log(Level.SEVERE, "-tvh, verilog heading statements option missing");
//				System.exit(20);
//			}
//			if (!cmd.hasOption("tvs")) {
//				logger.log(Level.SEVERE, "-tvs, verilog statements option missing");
//				System.exit(20);
//			}
			String temp = cmd.getOptionValue("t");
			File file = new File(temp);
			File files[] = null;
			if (file.isFile() && file.getName().endsWith(".v")) {
				files = new File[]{file};
			} else if (file.isDirectory()) {
				files = file.listFiles();
			} else {
				System.err.println(file.getAbsolutePath() + " is not directory");
			}
			if (files != null) {
				for (File tempFile : files) {
					if (tempFile.isFile() && tempFile.getName().endsWith(".v")) {
						String newContent = VerilogHelper.preProcess(tempFile, cmd.getOptionValue("tvh"), cmd.getOptionValue("tvs"));
						if (cmd.hasOption("o")) {
							File dir = new File(cmd.getOptionValue("o"));
							if (!dir.exists()) {
								dir.mkdirs();
							}
							File output = new File(dir.getAbsolutePath() + File.separator + tempFile.getName());
							FileUtils.write(output, newContent, "utf-8");
							logger.info("preprocessed " + tempFile.getName() + " to " + output.getAbsolutePath());
						} else {
							logger.info(newContent);
						}
					}
				}
			}
		} else if (cmd.hasOption("m")) {
			String temp = cmd.getOptionValue("m");
			File file = new File(temp);
			String content = IOUtils.toString(new FileInputStream(file), "utf-8");
			ParseStructure ps = new ParseStructure();
			String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, file, content, VerilogHelper.scanMacroDefinitions(file, file.getParentFile()), ps);
			System.out.println(sourceContent);
		}
	}

}
