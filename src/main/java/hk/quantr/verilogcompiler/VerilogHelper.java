package hk.quantr.verilogcompiler;

import hk.quantr.javalib.CommonLib;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.antlr.VerilogPreParser;
import hk.quantr.verilogcompiler.listener.AllModuleListener;
import hk.quantr.verilogcompiler.listener.SingleModuleListener;
import hk.quantr.verilogcompiler.listener.VerilogMacroDefinitionListener;
import hk.quantr.verilogcompiler.listener.preprocess.VerilogPreProcessorListener;
import hk.quantr.verilogcompiler.macro.MacroListing;
import hk.quantr.verilogcompiler.macro.MacroListing.Type;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogHelper {

	private static final Logger logger = Logger.getLogger(VerilogHelper.class.getName());

//	private static VerilogMacroDefinitionListener scanMacroDefinitions(File folder) throws FileNotFoundException, IOException {
//		logger.info("scanMacroDefinitions " + folder.getAbsolutePath());
//		VerilogMacroDefinitionListener listener = scanMacroDefinitions(null, folder);
//		return listener;
//	}
	public static VerilogMacroDefinitionListener scanMacroDefinitions(File preferFile, File folder) throws FileNotFoundException, IOException {
		logger.info(" --- scanMacroDefinitions " + preferFile + ", " + folder.getAbsolutePath());
		// scan all verilog for macro definitions
		logger.info(" --- listing folder " + folder.getAbsolutePath());
		Collection<File> temp = FileUtils.listFiles(folder, new String[]{"v"}, true);
		logger.info("s1");
		ArrayList<File> files = new ArrayList<>();
		logger.info("s2");
		files.addAll(temp);
		logger.info("s3");
		if (!preferFile.getParent().equals(folder.getAbsolutePath())) {
			files.add(0, preferFile);
		}
		logger.info("s4");
		VerilogMacroDefinitionListener listener = new VerilogMacroDefinitionListener();
		logger.info("s5");
		for (File file : files) {
			try {
				logger.info(" --- reading " + file.getAbsolutePath());
				VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(file), "utf-8")));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer, VerilogLexer.DIRECTIVES);
				VerilogPreParser parser = new VerilogPreParser(tokenStream);
				parser.addParseListener(listener);
				VerilogPreParser.Source_textContext context = parser.source_text();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		// end scan all verilog for macro definitions
		return listener;
	}

	public static VerilogMacroDefinitionListener getPreListener(String content) throws FileNotFoundException, IOException {
		VerilogMacroDefinitionListener verilogPreListener = new VerilogMacroDefinitionListener();
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer, VerilogLexer.DIRECTIVES);
		VerilogPreParser parser = new VerilogPreParser(tokenStream);
		parser.addParseListener(verilogPreListener);
		VerilogPreParser.Source_textContext context = parser.source_text();
		return verilogPreListener;
	}

	public static String getContentReplacedWithMacro(int parseNo, File file, String content, VerilogMacroDefinitionListener verilogPreListener, ParseStructure ps) throws FileNotFoundException, IOException {
		System.out.println("file=" + file.getAbsolutePath() + ", parseNo=" + parseNo);
		String lines[] = content.split("\r?\n");
		if (ps.macroListing.isEmpty()) {
			for (int x = 0; x < lines.length; x++) {
				ps.macroListing.add(new MacroListing(null, file, 0, x, x, lines[x], Type.ORIGINAL));
			}
		}

		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer, VerilogLexer.DIRECTIVES);
		tokenStream.fill();
		TokenStreamRewriter rewriter = new TokenStreamRewriter(tokenStream);
		VerilogMacroDefinitionListener preListener = getPreListener(content);
//		CommonLib.printWithLineNumber(content);
		int delta = 0;
		for (Token token : preListener.macroUsageTokens) {
			String macroText = token.getText();
//			System.out.println(">> " + parseNo + " " + macroText);
			String macroName = macroText.replaceAll("\\(.*", "");
			if (preListener.macroUsageTokens.contains(token) && verilogPreListener.macroDefinitions.get(macroName) != null) {
				String macroBody = verilogPreListener.macroDefinitions.get(macroName);
				if (macroText.contains("(")) {
					Pattern p = Pattern.compile("\\((.*)\\)");
					Matcher m = p.matcher(macroText);
					m.find();
					String callingParameters[] = m.group(1).split(",(?![^(){}]*[)}])\\s*");

					if (verilogPreListener.macroParameters != null) {
						if (verilogPreListener.macroParameters.get(macroName).size() == callingParameters.length) {
							for (int x = 0; x < verilogPreListener.macroParameters.get(macroName).size(); x++) {
								String parameter = verilogPreListener.macroParameters.get(macroName).get(x);
								String callingParamter = callingParameters[x];
								macroBody = macroBody.replaceAll("\\b" + parameter + "\\b", callingParamter);
							}
							int lineNo = token.getLine() - 1;
							MacroListing parentMacro = ps.get(lineNo, parseNo - 1);

							if (parentMacro == null) {
								System.err.println("ERROR 1: " + macroText);
								System.exit(125);
							}
							for (String temp : macroBody.split("\r?\n")) {
								parentMacro.children.add(new MacroListing(parentMacro, file, parseNo, parentMacro.originalLineNo, token.getLine() - 1 + delta, temp, Type.MACRO));
								delta++;
							}
							delta--;

							rewriter.replace(token, macroBody);
							rewriter.delete(token.getTokenIndex() - 1); // delete `
//							if (verilogPreListener.macroUsageTypes.get(macroName)) {
//								rewriter.delete(token.getTokenIndex() + 1); // remove last ';'
//							}
						} else {
							System.err.println("ERROR 2: " + macroText);
							System.exit(124);
						}
					} else {
						System.err.println("ERROR 3: " + macroText);
						System.exit(123);
					}
				} else {
					int lineNo = token.getLine() - 1;
					MacroListing parentMacro = ps.get(lineNo, parseNo - 1);
					if (parentMacro == null) {
						System.err.println("ERROR 4: " + macroText);
						System.exit(129);
					}
					parentMacro.children.add(new MacroListing(parentMacro, file, parseNo, parentMacro.originalLineNo, token.getLine() - 1 + delta, macroBody, Type.MACRO));

					rewriter.replace(token, macroBody);
					rewriter.delete(token.getTokenIndex() - 1); // delete `
				}
			}
		}

		String result = rewriter.getText();
		if (!result.equals(content)) {
//			System.out.println(ps);
//			System.out.println("+".repeat(100));
//			CommonLib.printWithLineNumber(result);
//			System.out.println("+".repeat(100));
			result = getContentReplacedWithMacro(parseNo + 1, file, result, verilogPreListener, ps);
		}
		return result;
	}

	public static String verilogToJson(File file) throws FileNotFoundException, IOException {
		if (!(file.isFile() && file.exists())) {
			logger.log(Level.SEVERE, "verilog file/folder {0} not exist", file.getAbsolutePath());
			return null;
		}

		ArrayList<hk.quantr.verilogcompiler.structure.Module> outputs = new ArrayList<>();
		if (file.isFile()) {
			outputs.add(fileToModule(file));
		} else if (file.isDirectory()) {
			Collection<File> files = FileUtils.listFiles(file, new String[]{"v"}, true);
			for (File f : files) {
				outputs.add(fileToModule(file));
			}
		}
		return CommonLib.objectToJson(outputs);
	}

//	public static hk.quantr.verilogcompiler.structure.Module parseTopModule(File folder, File topModuleFile) {
////		File topModuleFile = new File(folder.getAbsolutePath() + "/" + topModuleFile.getAbsolutePath());
//
//		Collection<File> files = FileUtils.listFiles(folder, new String[]{"v"}, true);
//		HashMap<String, hk.quantr.verilogcompiler.structure.Module> modules = new HashMap<>();
//		for (File file : files) {
//			try {
//				logger.log(Level.INFO, "parseTopModule(), processing {0}", file.getAbsolutePath());
//				String content = IOUtils.toString(new FileInputStream(file), "utf-8");
//				ParseStructure ps = new ParseStructure();
//				String processedContent = VerilogHelper.preProcess(file, null, null);
//				String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, file, processedContent, VerilogHelper.scanMacroDefinitions(file, folder), ps);
//				VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
//				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//				SingleModuleListener listener = new SingleModuleListener();
//				VerilogParser parser = new VerilogParser(tokenStream);
////				parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
//				parser.addParseListener(listener);
//				VerilogParser.Source_textContext context = parser.source_text();
//
//				if (listener.moduleName == null) {
//					continue;
//				}
//
//				modules.put(listener.moduleName, new hk.quantr.verilogcompiler.structure.Module(listener.moduleName, listener.inputs, listener.outputs));
//			} catch (IOException | RecognitionException ex) {
//				ex.printStackTrace();
//			}
//		}
//		logger.info("----");
//
//		// parse top module
//		try {
//			String content = IOUtils.toString(new FileInputStream(topModuleFile), "utf-8");
//			ParseStructure ps = new ParseStructure();
//			String processedContent = VerilogHelper.preProcess(topModuleFile, null, null);
//			String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, topModuleFile, processedContent, VerilogHelper.scanMacroDefinitions(topModuleFile, folder), ps);
//			VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
//			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//			AllModuleListener listener = new AllModuleListener(modules);
//			VerilogParser parser = new VerilogParser(tokenStream);
//			parser.addParseListener(listener);
//			VerilogParser.Source_textContext context = parser.source_text();
//
//			hk.quantr.verilogcompiler.structure.Module topModule = new hk.quantr.verilogcompiler.structure.Module();
//			topModule.name = listener.moduleName;
//			topModule.modules = listener.modules;
//			topModule.inputs = listener.inputs;
//			topModule.outputs = listener.outputs;
//			topModule.wires = listener.wires;
//
////			logger.info(topModule);
//			return topModule;
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//		return null;
//	}
	public static hk.quantr.verilogcompiler.structure.Module fileToModule(File file) throws IOException {
		String content = IOUtils.toString(new FileInputStream(file), "utf-8");
		ParseStructure ps = new ParseStructure();
		String sourceContent = VerilogHelper.getContentReplacedWithMacro(1, file, content, VerilogHelper.scanMacroDefinitions(file, file.getParentFile()), ps);
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		SingleModuleListener listener = new SingleModuleListener();
		VerilogParser parser = new VerilogParser(tokenStream);
		parser.addParseListener(listener);
		VerilogParser.Source_textContext context = parser.source_text();
		hk.quantr.verilogcompiler.structure.Module output = new hk.quantr.verilogcompiler.structure.Module();
		output.name = listener.moduleName;
		output.inputs = listener.inputs;
		output.outputs = listener.outputs;
		return output;
	}

	public static String preProcess(File file, String verilogheadingStatements, String verilogStatements) throws FileNotFoundException, FileNotFoundException, IOException {
		ParseStructure ps = new ParseStructure();
		String content = IOUtils.toString(new FileInputStream(file), "utf-8");
		String sourceContent = VerilogHelper.getContentReplacedWithMacro(1, file, content, VerilogHelper.scanMacroDefinitions(file, file.getParentFile()), ps);
//		FileUtils.writeStringToFile(new File("aaaa.v"), sourceContent, "utf-8");
//		CommonLib.printWithLineNumber(sourceContent);
		System.out.println(ps);
		System.out.println("+".repeat(100));
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		VerilogParser parser = new VerilogParser(tokenStream);
		ParseTree tree = parser.source_text();
//		System.out.println("-".repeat(100));

		// add our own macro
		VerilogPreProcessorListener listener = new VerilogPreProcessorListener(tokenStream, file, sourceContent, ps);
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(listener, tree);
//		System.out.println(ps);

		List<MacroListing> list = ps.fatternMacroListing().stream().filter(c -> c.type == Type.PROFILING).sorted((MacroListing o1, MacroListing o2) -> Integer.valueOf(o2.lineNo).compareTo(o1.lineNo)).collect(Collectors.toList());
		List<String> lines = new ArrayList(Arrays.asList(content.split("\r?\n")));
		for (MacroListing temp : list) {
//			System.out.println(temp + " = " + temp.getTopParent());
			lines.add(temp.getTopParent().originalLineNo + 1, temp.line);
		}

		return verilogheadingStatements + "\n" + String.join(System.lineSeparator(), lines) + "\n";
	}
}
