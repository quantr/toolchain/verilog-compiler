package hk.quantr.verilogcompiler.listener;

import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.antlr.VerilogParser.Module_instanceContext;
import hk.quantr.verilogcompiler.antlr.VerilogParser.Named_port_connectionContext;
import hk.quantr.verilogcompiler.antlr.VerilogParser.Port_identifierContext;
import hk.quantr.verilogcompiler.antlr.VerilogParserBaseListener;
import hk.quantr.verilogcompiler.structure.Port;
import hk.quantr.verilogcompiler.structure.Wire;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class AllModuleListener extends VerilogParserBaseListener {

	public ArrayList<Port> inputs = new ArrayList<>();
	public ArrayList<Port> outputs = new ArrayList<>();
	public TreeMap<String, Wire> wires = new TreeMap<>();
	public LinkedHashMap<String, hk.quantr.verilogcompiler.structure.Module> modules = new LinkedHashMap<>();

	private HashMap<String, hk.quantr.verilogcompiler.structure.Module> supportingModule;
	public String moduleName;

	public AllModuleListener(HashMap<String, hk.quantr.verilogcompiler.structure.Module> modules) {
		this.supportingModule = modules;
	}

	@Override
	public void exitInput_declaration(VerilogParser.Input_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			inputs.add(new Port("input", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitOutput_declaration(VerilogParser.Output_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		if (ctx.list_of_port_identifiers() != null) {
			List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
			for (Port_identifierContext portCtx : list) {
				outputs.add(new Port("output", type, range, portCtx.getText()));
			}
		}
		if (ctx.list_of_variable_port_identifiers() != null) {
			List<Port_identifierContext> list = ctx.list_of_variable_port_identifiers().port_identifier();
			for (Port_identifierContext portCtx : list) {
				outputs.add(new Port("output", type, range, portCtx.getText()));
			}
		}
	}

	@Override
	public void exitNet_declaration(VerilogParser.Net_declarationContext ctx) {
//		try {
		//		System.out.println(">>" + ctx.net_type().getText() + ", "
//				+ (ctx.drive_strength() == null ? "null" : ctx.drive_strength().getText()) + ",\t"
//				+ (ctx.delay3() == null ? "null" : ctx.delay3().getText()) + ",\t"
//				+ (ctx.range_() == null ? "null" : ctx.range_().getText()) + ",\t"
//				+ (ctx.list_of_net_decl_assignments() == null ? "null" : ctx.list_of_net_decl_assignments().getText()) + ",\t"
//				+ (ctx.list_of_net_identifiers() == null ? "null" : ctx.list_of_net_identifiers().getText())
//		);
		String wireName = ctx.list_of_net_identifiers() == null ? "null" : /*CommonLib.randomString(6, true, false, false) + "_" + */ ctx.list_of_net_identifiers().getText();
//		System.out.println("wireName=" + wireName);
		String wireType = ctx.range_() == null ? "null" : ctx.range_().getText();
		System.out.println(wireName);
		wires.put(wireName, new Wire(wireName, wireType));
//		} catch (OptionErrorException ex) {
//			Logger.getLogger(AllModuleListener.class.getName()).log(Level.SEVERE, null, ex);
//		}
	}

	@Override
	public void exitModule_instantiation(VerilogParser.Module_instantiationContext ctx) {
		moduleName = ctx.module_identifier().getText();
		try {
			for (Module_instanceContext module_instance : ctx.module_instance()) {
				System.out.println("moduleName=" + moduleName);
				hk.quantr.verilogcompiler.structure.Module module = supportingModule.get(moduleName);
				modules.put(module.name, module);

//			System.out.println("++" + ctx.module_identifier().getText() + ",\t" + module_instance.name_of_instance().getText() + ",\t" + module_instance.list_of_port_connections().getText());
				for (Named_port_connectionContext namedPort : module_instance.list_of_port_connections().named_port_connection()) {
					String portName = namedPort.port_identifier().getText();
					String wireName = namedPort.expression().getText();
//				System.out.println("name=" + namedPort.port_identifier().getText() + " = " + namedPort.expression().getText());
					Wire wire = wires.get(wireName);
					if (wire != null) {
						Port port = module.getPort(portName);
						if (port == null) {
							int xxxx = 123;
						}
						port.module = module;
						wire.ports.add(port);
						wire.modules.add(module);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ctx.getText());
			System.exit(1);
		}
	}
}
