package hk.quantr.verilogcompiler.listener;

import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.antlr.VerilogParser.Port_identifierContext;
import hk.quantr.verilogcompiler.antlr.VerilogParserBaseListener;
import hk.quantr.verilogcompiler.structure.Port;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SingleModuleListener extends VerilogParserBaseListener {

	public ArrayList<Port> inputs = new ArrayList<>();
	public ArrayList<Port> outputs = new ArrayList<>();
	public String moduleName;

	@Override
	public void exitInput_declaration(VerilogParser.Input_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			inputs.add(new Port("input", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitOutput_declaration(VerilogParser.Output_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		if (ctx.list_of_port_identifiers() != null) {
			List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
			for (Port_identifierContext portCtx : list) {
				outputs.add(new Port("output", type, range, portCtx.getText()));
			}
		}
		if (ctx.list_of_variable_port_identifiers() != null) {
			List<Port_identifierContext> list = ctx.list_of_variable_port_identifiers().port_identifier();
			for (Port_identifierContext portCtx : list) {
				outputs.add(new Port("output", type, range, portCtx.getText()));
			}
		}
	}

	@Override
	public void exitModule_declaration(VerilogParser.Module_declarationContext ctx) {
		moduleName = ctx.module_identifier().getText();
	}

}
