package hk.quantr.verilogcompiler.listener;

import hk.quantr.verilogcompiler.antlr.VerilogMacroBaseListener;
import hk.quantr.verilogcompiler.antlr.VerilogMacroParser;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompileListener extends VerilogMacroBaseListener {

	@Override
	public void exitMacro_body(VerilogMacroParser.Macro_bodyContext ctx) {
		System.out.println(ctx.name.getText() + " = " + ctx.module_name(0).getText());
	}
}
