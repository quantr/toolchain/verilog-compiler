package hk.quantr.verilogcompiler.listener;

import hk.quantr.verilogcompiler.VerilogCompiler;
import hk.quantr.verilogcompiler.antlr.VerilogPreParser;
import hk.quantr.verilogcompiler.antlr.VerilogPreParserBaseListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogMacroDefinitionListener extends VerilogPreParserBaseListener {

	private static Logger logger = Logger.getLogger(VerilogMacroDefinitionListener.class.getName());

	static {
		InputStream stream = VerilogMacroDefinitionListener.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public HashMap<String, String> macroDefinitions = new HashMap<>();
	public HashMap<String, ArrayList<String>> macroParameters = new HashMap<>();
	public ArrayList<Token> macroUsageTokens = new ArrayList<>();
	public HashMap<String, Boolean> macroUsageTypes = new HashMap<>();

	@Override
	public void exitText_macro_definition(VerilogPreParser.Text_macro_definitionContext ctx) {
		String identifier = ctx.text_macro_identifier().getText();
//		System.out.println(ctx.start.getLine() + "\t" + identifer);
		logger.info(" ------ read macro " + identifier);
		String body = ctx.macro_text().getText().replaceAll("//.*", "").trim();
//		System.out.println(body);
//		System.out.println("-".repeat(100));
		if (body.contains("(")) {
			// extract parameters
			String temp = body;
			temp = temp.replaceAll("[ \t]+", "");
			Pattern p = Pattern.compile("\\((.*?)\\)");
			Matcher m = p.matcher(temp);
			if (m.find()) {
				String parameters = m.group(1);
//				for (String parameter : parameters.split(",")) {
//					System.out.println(">" + parameter + "<");
//				}
				macroParameters.put(identifier, new ArrayList<>(Arrays.asList(parameters.split(","))));
			}
			// end extract parameters

			body = body.replaceFirst("\\([^)]*\\)", "");
			body = body.replaceAll("\\\\", "");
			body = body.trim();

			if (!macroDefinitions.containsKey(identifier)) {
				macroDefinitions.put(identifier, body);
				macroUsageTypes.put(identifier, true);
			}
		} else {
//			System.out.println(identifer + "\t=>\t" + body);
			if (!macroDefinitions.containsKey(identifier)) {
				macroDefinitions.put(identifier, body);
				macroUsageTypes.put(identifier, false);
			}
		}
	}

	@Override
	public void exitText_macro_usage(VerilogPreParser.Text_macro_usageContext ctx) {
//		System.out.println("usage: " + ctx.getStop());
		macroUsageTokens.add(ctx.getStop());
	}
}
