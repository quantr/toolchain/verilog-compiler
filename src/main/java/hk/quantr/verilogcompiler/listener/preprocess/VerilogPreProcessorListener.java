/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.listener.preprocess;

import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.antlr.VerilogParserBaseListener;
import hk.quantr.verilogcompiler.macro.MacroListing;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.TokenStreamRewriter;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogPreProcessorListener extends VerilogParserBaseListener {

	public TokenStreamRewriter rewriter;
	File file;
	String sourceContent;
	ParseStructure ps;

	public VerilogPreProcessorListener(TokenStream tokens, File file, String sourceContent, ParseStructure ps) {
		rewriter = new TokenStreamRewriter(tokens);
		this.file = file;
		this.sourceContent = sourceContent;
		this.ps = ps;
	}

	@Override
	public void exitBlocking_assignment(VerilogParser.Blocking_assignmentContext ctx) {
//		String line = sourceContent.split("\n")[ctx.start.getLine() - 1];
//		line = line.substring(0, ctx.start.getCharPositionInLine());
//		line = line.replaceAll("[^\t]", " ");
//		int noOfTab = StringUtils.countMatches(line, "\t");

		int lineNo = ctx.stop.getLine() - 1;
//		System.out.println(lineNo);
//		System.out.println(">>>>" + lineNo + " = " + ctx.getText());

		MacroListing parentMacro = ps.getByLastLineNo(lineNo, ps.getMaxParseNo());
		if (parentMacro == null) {
			System.out.println("parentMacro is null, " + lineNo);
			System.exit(929);
		}
		String verilogStatements = "";
//		verilogStatements += "$display(\"%x\", add(1,2));\n";
//		verilogStatements += "profile(\"" + ctx.variable_lvalue().getText() + "\");\n";
		verilogStatements += "/* verilator lint_off WIDTH */\n";
		verilogStatements += "profile(\"" + parentMacro.file.getAbsolutePath() + "\", " + (parentMacro.originalLineNo + 1) + ", \"" + ctx.variable_lvalue().getText() + "\", " + ctx.variable_lvalue().getText() + ");\n";
		verilogStatements += "/* verilator lint_on WIDTH */\n";
		parentMacro.children.add(new MacroListing(parentMacro, file, parentMacro.parseNo + 1, lineNo, parentMacro.originalLineNo, verilogStatements, MacroListing.Type.PROFILING));
	}

	@Override
	public void exitNonblocking_assignment(VerilogParser.Nonblocking_assignmentContext ctx) {
//		rewriter.insertAfter(ctx.stop, ";\n" + " ".repeat(ctx.start.getCharPositionInLine()) + "$display(" + ctx.variable_lvalue().getText() + ")");
	}
}
