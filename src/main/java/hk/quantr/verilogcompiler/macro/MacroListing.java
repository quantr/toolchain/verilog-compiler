/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.macro;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MacroListing {

	public enum Type {
		ORIGINAL, MACRO, PROFILING
	};

	public File file;
	public int parseNo;
	public Type type;
	public int originalLineNo;
	public int lineNo;
	public String line;
	public ArrayList<MacroListing> children = new ArrayList<>();
	public MacroListing parent;

	public MacroListing(MacroListing parent, File file, int parseNo, int originalLineNo, int lineNo, String line, Type type) {
		this.parent = parent;
		this.file = file;
		this.parseNo = parseNo;
		this.originalLineNo = originalLineNo;
		this.lineNo = lineNo;
		this.line = line;
		this.type = type;
	}

	@Override
	public String toString() {
//		if (type == Type.MACRO) {
//			return lineNo + "\t" + secondLineNo + "\t" + type + "(" + uuid + ")" + "\t" + line;
//		} else {
		return originalLineNo + "    " + lineNo + "    " + type + "    " + line.trim();
//		}
	}

	public boolean isEmptyIgnoreProfiling() {
		long count = children.stream().filter(c -> c.type != Type.PROFILING).count();
		return count == 0;
	}

	public MacroListing getTopParent() {
		if (parent == null) {
			return this;
		} else {
			return parent.getTopParent();
		}
	}

}
