/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.macro;

import hk.quantr.verilogcompiler.macro.MacroListing.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ParseStructure {

	public ArrayList<MacroListing> macroListing = new ArrayList<>();

	public Pair<String, Integer> macroListingToString(ArrayList<MacroListing> macroListingPara, int index) {
		String s = "";
		for (MacroListing temp : macroListingPara) {
			s += index + "\t(" + temp.parseNo + ") +" + "----".repeat(temp.parseNo + 1) + " " + temp + "\n";
			index++;
			if (temp.children.isEmpty()) {
			} else {
				Pair<String, Integer> temp2 = macroListingToString(temp.children, index);
				s += temp2.getLeft();
				index = temp2.getRight();
			}
		}
		return new ImmutablePair(s, index);
	}

	@Override
	public String toString() {
		return macroListingToString(macroListing, 0).getLeft();
	}

	public MacroListing get(int lineNo, int parseNo) {
		return get(lineNo, parseNo, macroListing);
	}

	public MacroListing get(int lineNo, int parseNo, ArrayList<MacroListing> temp) {
		for (MacroListing m : temp) {
			if (m.lineNo == lineNo && m.parseNo == parseNo) {
				return m;
			}
			if (!m.children.isEmpty()) {
				MacroListing p = get(lineNo, parseNo, m.children);
				if (p != null) {
					return p;
				}
			}
		}
		return null;
	}

	private void addAll(ArrayList<MacroListing> result, ArrayList<MacroListing> temp, boolean leafOnly) {
		for (MacroListing t : temp) {
			if (t.type == Type.PROFILING) {
				continue;
			}
			long count = t.children.stream().filter(c -> c.type != Type.PROFILING).count();

			if (!leafOnly || (count == 0 && leafOnly)) {
				result.add(t);
			}
			addAll(result, t.children, leafOnly);
		}
	}

	public int getMaxParseNo() {
		int parseNo = -1;
		for (MacroListing temp : fatternMacroListing()) {
			if (temp.type != Type.PROFILING && temp.parseNo > parseNo) {
				parseNo = temp.parseNo;
			}
		}
		return parseNo;
	}

//	public ArrayList<MacroListing> fatternMacroListing(boolean leafOnly) {
//		ArrayList<MacroListing> result = new ArrayList<>();
//		addAll(result, macroListing, leafOnly);
//		return result;
//	}
	private void addAll(ArrayList<MacroListing> result, ArrayList<MacroListing> temp) {
		for (MacroListing t : temp) {
			result.add(t);
			addAll(result, t.children);
		}
	}

	public ArrayList<MacroListing> fatternMacroListing() {
		ArrayList<MacroListing> result = new ArrayList<>();
		addAll(result, macroListing);
		return result;
	}

	public MacroListing getByLastLineNo(int lineNo, int parseNo) {
//		for (MacroListing t : fatternMacroListing()) {
//			System.out.println("- " + t);
//		}
		List<MacroListing> temp = fatternMacroListing().stream().filter(c -> c.type != Type.PROFILING && c.isEmptyIgnoreProfiling()).collect(Collectors.toList());
//		for (MacroListing t : temp) {
//			System.out.println("+ " + t);
//		}
		int bingoX = -1;
//		if (lineNo == 748) {
//			System.out.println("");
//		}
		for (int x = temp.size() - 1; x >= 0; x--) {
//			if (x == 130) {
//				System.out.println("");
//			}
			if (temp.get(x).lineNo <= lineNo && temp.get(x).parseNo == parseNo) {
				bingoX = x;
				break;
			}
		}
		if (bingoX == -1) {
			System.err.println("bingoX is -1");
			System.exit(1699);
		}
		int delta;
		if (temp.get(bingoX).isEmptyIgnoreProfiling()) {
			delta = lineNo - temp.get(bingoX).lineNo;
		} else {
			delta = lineNo - temp.get(bingoX).lineNo - 1;
		}
		if (delta == 0) {
			return temp.get(bingoX);
		} else {
			int tempDelta = bingoX + 1;
			int lastLineNo = -99999;
			MacroListing deltaMacroListing;
			do {
				deltaMacroListing = temp.get(tempDelta);
				if (deltaMacroListing.children.stream().filter(c -> c.type != Type.PROFILING).count() == 0 && lastLineNo != deltaMacroListing.lineNo) {
					delta--;
				}
				lastLineNo = deltaMacroListing.lineNo;
				tempDelta++;
			} while (delta != 0);

//			MacroListing deltaMacroListing = temp.get(tempDelta);
//			int lastLineNo = deltaMacroListing.lineNo;
//			while (delta > 0) {
//				if (deltaMacroListing.children.stream().filter(c -> c.type != Type.PROFILING).count() == 0 && lastLineNo != deltaMacroListing.lineNo) {
//					delta--;
//				}
//				tempDelta++;
//				lastLineNo = deltaMacroListing.lineNo;
//				deltaMacroListing = temp.get(tempDelta);
//			}
//		return temp.get(bingoX + delta);
			return deltaMacroListing;
		}
	}

}
