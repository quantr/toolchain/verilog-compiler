/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.structure;

import java.io.Serializable;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Port implements Serializable {

	public String direction;
	public String type;
	public String range;
	public String name;
	public Module module;

	public Port(String direction, String type, String range, String name) {
		this.direction = direction;
		this.type = type;
		this.range = range;
		this.name = name;
	}

	@Override
	public String toString() {
		if (module == null) {
			return "Port{name=" + name + ", direction=" + direction + ", type=" + type + ", range=" + range + "}";
		} else {
			return "Port{name=" + name + ", direction=" + direction + ", module=" + module.name + ", type=" + type + ", range=" + range + "}";
		}
	}

}
