/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.structure;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Wire implements Serializable {

	public String name;
	public String type;

	public ArrayList<hk.quantr.verilogcompiler.structure.Module> modules = new ArrayList<>();
	public ArrayList<Port> ports = new ArrayList<>();

	public Wire(String name, String type) {
		this.name = name;
		this.type = type;
	}

	@Override
	public String toString() {
		String temp = "";
		for (Port port : ports) {
			temp += ", " + port;
		}
		return "Wire{" + "name=" + name + ", type=" + type + temp + '}';
	}

}
