
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class GetAllHiddenTokens {

	@Test
	public void test() {
		String content = "`define RESET 1'b1\n"
				+ "`define MXLEN 63:0\n"
				+ "`define INST_WIDTH 31:0\n"
				+ "`define REG_WIDTH 4:0\n"
				+ "`define ZeroWord 32'h00000000\n"
				+ "`define ZeroDWord 64'h0000000000000000\n"
				+ "`define InstMemNum 131071\n"
				+ "`define InstMemNumLog2 17\n"
				+ "//@{connect(moduleA, moduleB)} @{connect(moduleA, moduleB)}\n"
				+ "`define AluOpBus 7:0 // hello peter\n"
				+ "`define AluSelBus 2:0\n"
				+ "`define EXE_OR_OP    8'b00100101\n"
				+ "\n"
				+ "`define EXE_NOP_OP 8'b0\n"
				+ "`define EXE_RES_NOP 3'b0\n"
				+ "`define WriteDisable 1'b0\n"
				+ "`define EXE_RES_LOGIC 3'b001";
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer, 3);
		tokenStream.fill();
//		MyVerilogListener listener = new MyVerilogListener();
//		Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
//		parser.addParseListener(listener);
//		Verilog2001Parser.Source_textContext context = parser.source_text();

		for (Token token : tokenStream.getTokens()) {
			if (token.getChannel() == VerilogLexer.HIDDEN && !token.getText().trim().equals("")) {
				int lineNo = token.getLine();
				int startIndex = token.getStartIndex();
				int stopIndex = token.getStopIndex();

				System.out.println(VerilogLexer.VOCABULARY.getDisplayName(token.getType()) + " = " + VerilogLexer.channelNames[token.getChannel()] + " = " + token.getText().replaceAll("\n", "") + " = " + token.getLine() + " , " + token.getStartIndex());
			}
		}
	}
}
