
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import hk.quantr.verilogcompiler.antlr.VerilogPreParser;
import hk.quantr.verilogcompiler.listener.VerilogMacroDefinitionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestMacroUsage {

	@Test
	public void test() throws IOException {
		// scan all verilog for macro definitions
		VerilogMacroDefinitionListener verilogPreListener = new VerilogMacroDefinitionListener();
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(new File("../quantr-i/src/ex.v")), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer, VerilogLexer.DIRECTIVES);
		VerilogPreParser parser = new VerilogPreParser(tokenStream);
		parser.addParseListener(verilogPreListener);
		VerilogPreParser.Source_textContext context = parser.source_text();
		// end scan all verilog for macro definitions
	}
}
