
import hk.quantr.verilogcompiler.VerilogHelper;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/*
 * Quantr Commercial License
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestNetbeansVerilog {

	@Test
	public void test() throws IOException {
//		String newContent = VerilogHelper.preProcess(new File("../quantr-i/src/ctrl.v"), "", null);
//		System.out.println(newContent);

		File file = new File("../quantr-i/src/ctrl.v");
		String content = FileUtils.readFileToString(file, "utf-8");
		File folder = new File("../quantr-i/src");
		System.out.println("folder=" + folder.getAbsolutePath());
		ParseStructure ps = new ParseStructure();
//		VerilogMacroDefinitionListener listener = VerilogHelper.scanMacroDefinitions(folder);
//		for (String key : listener.macroDefinitions.keySet()) {
//			System.out.println(key + " = " + listener.macroDefinitions.get(key));
//		}
//		String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, content, listener, ps);
		String sourceContent = VerilogHelper.getContentReplacedWithMacro(1, file, content, VerilogHelper.scanMacroDefinitions(file, file.getParentFile()), ps);

		System.out.println(sourceContent);
	}
}
