
import hk.quantr.javalib.CommonLib;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.VerilogHelper;
import hk.quantr.verilogcompiler.listener.SingleModuleListener;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import java.io.FileInputStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSingleModuleParser {

	@Test
	public void test() throws Exception {
		// replace source code with marco
		File folder = new File("../quantr-i/src");
		File file = new File("../quantr-i/src/ex_mem.v");
		String content = IOUtils.toString(new FileInputStream(file), "utf-8");
		ParseStructure ps = new ParseStructure();
		String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, file, content, VerilogHelper.scanMacroDefinitions(file, folder), ps);
		CommonLib.printWithLineNumber(sourceContent);

		// end replace source code with marco
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		SingleModuleListener listener = new SingleModuleListener();
		VerilogParser parser = new VerilogParser(tokenStream);
		parser.addParseListener(listener);
		VerilogParser.Source_textContext context = parser.source_text();
		System.out.println("module=" + listener.moduleName);
	}
}
