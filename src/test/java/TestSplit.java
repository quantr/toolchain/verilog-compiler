
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSplit {

	@Test
	public void test() {
//		String str = "`EXE_RES_SHIFT,`EXE_I_SLL,1,0,rs1,rs2,1,rd,{58'd0,inst_i[25:20]}";
		String str = "1,(pc_i+ {{44{inst_i[31]}},inst_i[19:12],inst_i[20],inst_i[30:21], 1'b0}),next_pc";
		String temp[] = str.split(",(?![^(){}]*[)}])\\s*");
		for (String x : temp) {
			System.out.println(x);
		}
	}
}
