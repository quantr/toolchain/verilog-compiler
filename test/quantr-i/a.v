module temp;
    `define MEMWB_SETTINGS(we, wa, wdata, cycle) \
        wb_wreg     <= we; \
        wb_wd       <= wa; \
        wb_wdata    <= wdata; \
        stall_cycle <= cycle;

always @(*) begin
        wb_wreg     <= we;
        wb_wd       <= wa;
        wb_wdata    = wdata;
        stall_cycle <= cycle;
end
endmodule