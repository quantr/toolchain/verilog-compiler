`define RESET 1'b1
`define MXLEN 63:0
`define INST_WIDTH 31:0
`define REG_WIDTH 4:0
`define ZeroWord 32'h00000000
`define ZeroDWord 64'h0000000000000000
`define InstMemNum 131071
`define InstMemNumLog2 17
`define AluOpBus 7:0
`define AluSelBus 2:0

// opcode @{connect(moduleA, moduleB, moduleC)}
`define EXE_OR_OP    8'b00100101

//AluSel
`define EXE_NOP_OP 8'b0
`define EXE_RES_NOP 3'b0
`define WriteDisable 1'b0
`define EXE_RES_LOGIC 3'b001
