module id(input wire rst,
          output reg wreg_o);

    wire[6:0] opcode = inst_i[6:0];
    wire[2:0] funct3 = inst_i[14:12];
    
    always @(*) begin
        if (rst == `RESET) begin
            aluop_o     = 8'b00000000;
            alusel_o    = 3'b000;

			if (opcode == 7'b0110011 && funct3 == 3'd6) begin
				wreg_o      = 1'b1;
				aluop_o     = `EXE_OR_OP;
			   	alusel_o    = `EXE_RES_LOGIC;
				   reg1_read_o = 1'b1;
         				reg2_read_o = 1'b0;
				imm         = {48'h0, inst_i[15:0]};
				wd_o        = inst_i[20:16];
				instvalid   = 1'b0;
			end
		end
    end
    
endmodule
