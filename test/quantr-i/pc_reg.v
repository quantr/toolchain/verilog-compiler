module pc_reg(input wire clk,
              input wire rst,
              output reg[`MXLEN] pc);
    always @ (posedge clk) begin
        if (rst == `RESET) begin
            pc <= `ZeroDWord;
            end else begin
            pc <= pc+4;
        end
    end
endmodule
