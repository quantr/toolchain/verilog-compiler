/* verilator lint_off UNUSED */

module rom(input wire rst,
           input wire[`MXLEN] addr,
           output reg[`INST_WIDTH] inst);
    
    reg[`INST_WIDTH] inst_mem[0:`InstMemNum-1];
    
    initial $readmemh("rom.data", inst_mem);
    
    always @ (*) begin
        if (rst == `RESET) begin
            inst = `ZeroWord;
            end else begin
            inst = inst_mem[addr[`InstMemNumLog2+1:2]];
        end
    end
    
endmodule
